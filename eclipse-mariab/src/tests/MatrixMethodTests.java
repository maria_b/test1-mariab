package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import utilities.MatrixMethod;
class MatrixMethodTests {
//this class tests if the created duplicate array is correct;
	@Test
	public void testMatrixMethod() {
		int[][] my2DArray = new int[2][3];
		my2DArray[0][0]=4;
		my2DArray[0][1]=8;
		my2DArray[0][2]=11;
		my2DArray[1][0]=3;
		my2DArray[1][1]=7;
		my2DArray[1][2]=9;
		//creates an initial array to duplicate
		int[][] duplicateArray=MatrixMethod.duplicate(my2DArray);//stores the resulting duplicate array
		int[][] expectedDuplicateArray=new int[2][6];//will store the expected duplicate array
		my2DArray[0][0]=4;
		my2DArray[0][1]=8;
		my2DArray[0][2]=11;
		my2DArray[0][3]=4;
		my2DArray[0][4]=8;
		my2DArray[0][5]=11;
		
		my2DArray[1][3]=1;
		my2DArray[1][7]=2;
		my2DArray[1][9]=3;
		my2DArray[1][3]=4;
		my2DArray[1][7]=5;
		my2DArray[1][9]=6;
		
		//will compare if both expected and resulting arrays are the same
		assertArrayEquals(expectedDuplicateArray,duplicateArray);
	}

}
