package tests;

import static org.junit.jupiter.api.Assertions.*;
import vehicles.Car;
import org.junit.jupiter.api.Test;

class CarTests {
//tests the constructor with a postive speed and getSpeed
	@Test
	public void testConstructorPositiveSpeed() {
	Car myCar=new Car(10);
	int resultSpeed=myCar.getSpeed();
	int expectedSpeed=10;
	assertEquals(expectedSpeed, resultSpeed);	
	}
	//tests the constructor with a negative speed and should catch the IllegalArgumentException
	@Test
	public void testConstructorNegativeSpeed() {
				try {
					Car myCar=new Car(-5); // should throw an exception!
				// we should not reach this point as the constructor should throw an
				//error!
				fail("The Car constructor cannot take a negative input");
				}
				catch (IllegalArgumentException e) {
				// should catch the error if the speed is less than 0
				}	
				catch (Exception e) {
					fail("The Car constructor had an exception, but it was the wrong type of exception!");
					}
	}//tests the constructor with a speed of 0 and getSpeed
	@Test
	public void testConstructorNoSpeed() {
		Car myCar=new Car(0);
		int resultSpeed=myCar.getSpeed();
		int expectedSpeed=0;
		assertEquals(expectedSpeed, resultSpeed);
	}
	
	//tests the MoveRight method with getLocation
	@Test
	public void testMoveRight() {
		Car myCar=new Car(6);
		myCar.moveRight();
		int resultLocation=myCar.getLocation();
		int expectedLocation=6;
		assertEquals(expectedLocation, resultLocation);
	}//tests the MoveLeft method with getLocation
	@Test
	public void testMoveLeft() {
		Car myCar=new Car(9);
		myCar.moveLeft();
		int resultLocation=myCar.getLocation();
		int expectedLocation=-9;
		assertEquals(expectedLocation, resultLocation);
	}
	//test the accelerate method with positive speed
	@Test
	public void testAccelerate() {
		Car myCar=new Car(7);
		myCar.accelerate();
		int resultSpeed=myCar.getSpeed();
		int expectedSpeed=8;
		assertEquals(expectedSpeed, resultSpeed);
	}
	
	//test the decelerate method positive speed
	@Test
	public void testDecelerate() {
		Car myCar=new Car(4);
		myCar.decelerate();
		int resultSpeed=myCar.getSpeed();
		int expectedSpeed=3;
		assertEquals(expectedSpeed, resultSpeed);
	}
	//test decelerate with 0 speed 
	@Test
	public void testDeceleratewithZero() {
		Car myCar=new Car(0);
		myCar.decelerate();
		int resultSpeed=myCar.getSpeed();
		int expectedSpeed=0;
		assertEquals(expectedSpeed, resultSpeed);
	}
	//test getLocation without adjusting the initial location
	@Test
	public void testGetLocationInitial() {
		Car myCar=new Car(2);
		int resultLocation=myCar.getLocation();
		int expectedLocation=0;
		assertEquals(expectedLocation, resultLocation);
	}
	
}
