package utilities;

public class MatrixMethod {
	/**public static void main(String [] args) {
		int[][] my2DArray = new int[2][];
		my2DArray[0] = new int[3];
		my2DArray[1] = new int[3];
		my2DArray[0][0]=1;
		my2DArray[0][1]=2;
		my2DArray[0][2]=3;
		my2DArray[1][0]=4;
		my2DArray[1][1]=5;
		my2DArray[1][2]=6;
		
		
		for(int i=0; i< my2DArray.length; i++) {
			for (int j=0; j<my2DArray[i].length; j++) {
				System.out.println("my 2d array");
				System.out.println(my2DArray[i][j]);
			}
			System.out.println("My 2D array length is "+ my2DArray.length);
		}
		int[][] duplicateArray=duplicate(my2DArray);
		for(int i=0; i< duplicateArray.length; i++) {
			for (int j=0; j<duplicateArray[i].length; j++) {
				System.out.println(duplicateArray[i][j]);
			}
		}
		}
		**/
	//this method creates a duplicate of the array , it repeats a second time the array inside the array.
	
	public static int[][] duplicate(int[][] aMatrix){	
		int [][] aMatrixDuplicate = new int [aMatrix.length][((aMatrix[1].length)*2)]  ;
		
		for(int i=0;i<aMatrix.length; i++) {//will loop through all the data
			int position=0;
			for(int j=0;j<aMatrixDuplicate[i].length;j++) {//will loop through the arrays inside arrays
				aMatrixDuplicate[i][j]=aMatrix[i][position];
				position++;
				if( position==aMatrixDuplicate[i].length/2) {
				position=0;	
				}
			}
		
	}
		return aMatrixDuplicate;		
}
}
